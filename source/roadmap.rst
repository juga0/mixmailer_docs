.. _roadmap:

Roadmap
-------

Overview of what is done so far:

- [X] Research on existing mix networks
- [X] Try existing mix networks and see how they could be adapted
- [X] Implement Proposal 1 including integration tests
- [X] Try GNUnet GNS and write tests
- [X] Document research, experiments, proposal

Next:

- [ ] Document threat model
- [ ] Write technical specification
- [ ] Implement the prototype, adapted to :ref:`p11`, in the pEp engine
