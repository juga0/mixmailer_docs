pEp MixMailer documentation
===========================

Work in progress documentation about pEp "MixMailer" project including
analysis, design and specification.

To build the documentation, install `Sphinx <https://www.sphinx-doc.org>`_ and
run ``make html``.
Optionally, you can build new .svg diagrams from .puml files installing
`plantUML <https://plantuml.com>` and running make plantuml.

Authors: juga at riseup dot net.
