#!/bin/bash

set -x

rm -rf build/html
make html
cd ../mixmailer_docs.pages
rm -rf *
rsync -av --exclude=".*" ../mixmailer_docs/build/html/ .
git add -A
git commit -m "Automatic build"
# git push
# cd ../mixmailer_docs
