.. _p2:

Proposal 2 (P2)
===============

:ref:`katzenpost` +  [Mailproxy]_

Mailproxy is a mailserver & mixnet client intended to run on the users’
devices. Not as a provider’s mail server, as this would create an
“entry-node” to the mixnet which defeats certain security guarantees.

The client would need to configure the SMTP socket address (IP:port) of the
mailproxy.

Outlook clients use [MAPI]_ . It's possible to use MAPI over HTTP.


Differences between loopix and katzenpost
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In loopix there’s only 3rd party anonymity (unlinkability), while
katzenpost introduced the concept on “ephemeral” providers, which are
the entry point for the client/user to query their “remote spool” to
retrieve/send the messages via Single Use Reply Blocks ( [katzenpostSURB]_ s).

This way in katzenpost there’s also sender and receiver anonymity
(unlinkability)

In loopix the user keys are also in the PKI, (are they also in
katzenpost?)
