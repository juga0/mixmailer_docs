Threat model
============

See :ref:`concepts_threat_model` for an idea on threat modeling.

This is the threat model on :ref:`p11`.

Arquitecture diagrams
---------------------

See `Nodes and messages <../_static/nodes_msgs.svg>`_,
`Deployment <../_static/deployment.svg>`_ and
`Deployment with GNS <../_static/deployment_gns.svg>`_

Threats on the software
-----------------------

Threats on the network transmission
-----------------------------------

See :ref:`attacks` on common attacks to anonymity systems.

Adversaries
~~~~~~~~~~~

The adversaries here are all the adversaries mentioned in
:ref:`concepts_adversaries`.

Vulnerabilities
~~~~~~~~~~~~~~~

Attacks to which :ref:`p11` is vulnerable:

- Replay Attacks
- Blending Attacks
- Passive subpoena attack
- Active subpoena attack
- Partition attack on client knowledge
- Tagging attack on headers
- Tagging attack on payload
- Attacks on multiple messages / large files:
  While messages are not splitted into packets, the packets don't have to end
  in the same node. But it's still vulnerable because of different sizes.
- Pseudospoofing
- Intersection attacks
- Timing and packet counting attacks:
  Even if padding is added, messages are still different sizes.

Mitigations
~~~~~~~~~~~
