.. _p11:

Proposal 1.1 (P11)
==================

aka pEp onion routing/remailer?

This is an attempt to transcribe V. ideas after juga's presentation on
January 12, 2021 [MixmailerSlides]_
and some other comments with N. and D.
These are not juga's ideas.

As D. noted, it should probably not be called mixnet.

This proposal very similar to :ref:`p1`. The main difference seems to be not
aiming to implement a mix network but just onion routing, which it is actually
simpler.

Any mention to prototype means this: [Mixmailer]_

Routing
-------

As in Proposal 1 (P1), client decide the route for the message.

The user might also decide.

Transport
---------

As in P1. SMTP, no TLS.

Nodes' keys
-----------

Same as in P1. Each node have its own private/public OpenPGP keys.

Keys rotation
-------------

Not defined

Key discovery (by clients and nodes)
------------------------------------

In P1 it was proposed that nodes/clients exchange messages to discover keys.
Then we started to use GNUnet :ref:`gns` in the prototype.
In this proposal we'll continue with GNS is used to query the list of nodes to
the authorities and other nodes' keys.
In the prototype it's explained in more detail: :ref:`ppm:gns`, though the part
about layers should be ignored.

Key registration
----------------

This is being researched by N.

Types of nodes
--------------

All the same. In the prototype juga decided thought it would be better to use
stratified topology, but it can be removed.

Topology
--------

Free-routing.

Packet format
-------------

No packets, just messages. Not really MIME Multipart Encrypted (and Signed?).
pEp message 2.x? [ref?].

Packet size
------------

Instead of padding in a way that the final messages are all the same few fixed
sizes, they'll be padded randomly.
If the final message is too big it'll be just discarded.
This is different to what was tried to implement in the prototype and P1.

Packet mixing & delays
----------------------

Undecided. If it's just onion routing, in principle there's no need for.
In the prototype there was just random delays.

Cover traffic
-------------

None.

Directory servers/authorities
-----------------------------

In P1 it was said that there was no need. Since we started to use GNS, they're
needed.
The prototype already assume there'll be authorities.

Spam
----

It doesn't exist.

Network diversity
-----------------

Community will be made.


Nodes and messages diagram
--------------------------

`Nodes and messages <../_static/nodes_msgs.svg>`_

.. image:: /_static/nodes_msgs.svg

Deployment diagram
------------------

As the one before, including software components.

`Deployment <../_static/deployment.svg>`_

.. image:: /_static/deployment.svg

Deployment diagram including GNS
--------------------------------

As the one before, including GNS.

`Deployment with GNS <../_static/deployment_gns.svg>`_

.. image:: /_static/deployment_gns.svg

See :ref:`proposals_arguments` for arguments in pro/con of the several
proposals.
