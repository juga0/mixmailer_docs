.. _p2p:

Peer to Peer networks
=====================

.. _gnunet:

GNUnet
------

   GNUnet is a software framework for decentralized, peer-to-peer
   networking and an official GNU package. The framework offers link
   encryption, peer discovery, resource allocation, communication over
   many transports (such as TCP, UDP, HTTP, HTTPS, WLAN and Bluetooth)
   and various basic peer-to-peer algorithms for routing, multicast and
   network size estimation.

   GNUnet's basic network topology is that of a mesh network. GNUnet
   includes a distributed hash table (DHT) which is a randomized variant
   of Kademlia that can still efficiently route in small-world networks.
   GNUnet offers a "F2F topology" option for restricting connections to
   only the users' trusted friends. The users' friends' own friends (and
   so on) can then indirectly exchange files with the users' computer,
   never using its IP address directly.

   GNUnet uses Uniform resource identifiers (not approved by IANA,
   although an application has been made).[when?] GNUnet URIs consist of
   two major parts: the module and the module specific identifier. A
   GNUnet URI is of form gnunet://module/identifier where module is the
   module name and identifier is a module specific string.

[gnunetGNUnet]_

   A distributed hash table (DHT) is a distributed system that provides
   a lookup service similar to a hash table[GNUnet]: (key, value) pairs are
   stored in a DHT, and any participating node can efficiently retrieve
   the value associated with a given key.

 [DHT]_

.. _gns:

GNS
~~~

   GNUnet includes an implementation of the GNU Name System (GNS), a
   decentralized and censorship-resistant replacement for DNS. In GNS,
   each user manages their own zones and can delegate subdomains to
   zones managed by other users. Lookups of records defined by other
   users are performed using GNUnet's DHT

[gnunetGNS]_
