.. _attacks:

Attacks
=======

Based on [AttacksAnonymitySystems]_ talk on 2003 that explained the design
of :ref:`type-iii-mixminion` to mitigate the vulnerabilities the previous
remailers: :ref:`type-ii-mixmaster` and :ref:`type-i-cypherpunks`.

Explanation of the attack, goal of the attack, which are the adversaries,
which are the capabilities or the adversarios and which the mitigations (M)

Distinguish different-size messages
-----------------------------------

A passive local adversary can guess which message coming out of a mix
corresponds to a message coming in just by the size.
They can guess also the position of the message in the path.

Mitigation: padding. Add random junk to the bottom to replace the header
that is removed.

Cyhperpunk remailer is vulnerable to this, but not mixmaster or mixminion.

Replay attack
-------------

An adversary copy the message and send it back, it will follow the same path.
It's traceable by a global passive adversary.
It helps local passive adversaries and rogue operators.
Depends on where the message is going, eg. same ISP.

Mitigation: add hash to the header and get nodes to cache the hashes. If an
incoming message is already in the cache, drop it.

Problem: nodes would need to remember cache forever.

Mitigation: add expiration date to the cache, eg. 3 days ago and 3 days from
now. If expiration of cache is more than 7 days old, drop it.
Adversaries can't tell when the message was sent from its expiration date.

Cyhperpunk remailer is vulnerable to this attack, but not mixmaster or
mixminion.

Flooding (Blending) attack
--------------------------

An active local adversary send many messages to detect a legit one.
Very effective attack in batch mixes, but not in pooling ones.
There're other cheaper and more effective attacks.

Mitigation: pooling mixnet.

Trickle attack (n-1)
--------------------

If there's only one user (1 legit message) at a given interval of time,
flooding attack is also effective in pooling mixes.

Mitigation: dummy decoy messages.

Problem: dropped at some node, only covers internal traffic.

Cyhperpunk remailer is vulnerable to this attack, but not mixmaster or
mixminion.

Passive subpoena attack
-----------------------

A local passive adversary copy message for a later subpoena.

Mitigation: encrypt link transmission (TLS)

Problem: TLS can be enforced in nodes running TLS and postfix doesn't rotate
keys too often.

Solution: enforce link transmission encryption at protocol level and have
effemeral keys that rotate often.

Cyhperpunk and mixmaster remailers are vulnerable to this attack, but not
mixminion.

Active subpoena attack
----------------------

A rogue operator can record messages for a later subpoena.

Mitigation: periodic key rotation.

Cyhperpunk and mixmaster remailers are vulnerable to this attack, but not
mixminion.


Partition attack on client knowledge
------------------------------------

Adversary make some users to connect to a different set of nodes that
it's easier to observe or control by the adversary.

Different versions of OpenPGP encrypt differently and looks different.
Also choosing different algorithms.
Watching users that are using a key about to expire (not that common).

It needs the atacker to do something else.
It can be statistical analysis over time.

Mitigation: Directory servers (DSs) have to be part of the specification to
avoid different DSs behaving differently.
They also have to sign bundles.
Problem: how can DSs agree on what to sign?

Question: having DSs is not P2P?
Answer: there're several DSs that talk to each other, so it's P2P in that sense
But there's an small number of DSs and it's static cause their public keys need
to be in the code.

Partition attack on message expiration date
-------------------------------------------

The adversary delays a message near to its expiration date, so that they can
recognize it cause because there're not that many.
Statistical attack, not deterministic.

Mitigation: No expiration dates, keep hashes until keys rotate.

Tagging attack on headers
-------------------------

An adversary flips bits in the encrypted payload at the possition of the header.
If the adversary owns a node later in the path that correspond to the broken
header, they can recognize the message.

Mitigation: hash covering the entire header.

Mixmaster is vulnerable to this attack.
There're easier and more effective attacks.

Tagging attack on payload
-------------------------
Adversary flips some bits in the payload, and try to recognize altered messages
when they’re delivered.

Mitigation: Make the hash cover the payload too.

Attack on multiple messages
---------------------------

Large messages that are splitted into packets, they can have different routes,
but they all need to end in the same last remailer to reconstruct the message.

A passive global adversary observing only input and output of the mixnet can
correlate the messages by number of them.
Users can only "hide" with other users using the net in the same way, eg.
messages containing a vote or pictures have different patterns.
Also passive local adversaries can gather enough information observing the net
enough time.

Very effective attack: don't send large files unless every body else does it
too.

Cyhperpunk, mixmaster and mixminion are vulnerable to this attack.

Pseudospoofing
--------------

Rogue operator running several nodes.
In mixmaster an operator can join the network with human interaction, though
the operator can have different personas and operate large part of the network.
In mixminion there's no human interaction.
Tor uses "families", scripts, authorities and humans to detect these cases.
Ratio of users per node, not clear
If user retrieves the nodes they are going to use by pinging them, then it's
lear which nodes they're going to use.


Intersection attacks
---------------------

Adversaries can know who are the users over time, cause the tend to do the same
thing more than once. Over time it's possible to know who they are.
eg. every morning a user sends message to same receiver, except when they are
on holidays.
Difficult except for a global passive adversary or rogue operator that happens
to be consistenly the 1st and last hop.

Timing and packet counting attacks
----------------------------------

Adversary do statistical analysis of network traffic.
Mostly on low-latency systems with pool of messages.

Mitigation: padding. Even if timing analysis is harder with delays, the system
can not add too much delay to keep it usable.

Bottleneck compromises
----------------------

In a nym server system obviously the nym server is a Bottleneck since all the
traffic goes throug it.

A node is more reliable might be used more often and therefore attacking/owning
easies attacks.
Trade-off between having users choose reliable servers or servers in one
jurisdiction.
