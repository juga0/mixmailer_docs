.. _references:

References
==========

On topics mentioned in this documentation.

Papers and RFCs
---------------

.. [AnonTerms] http://dud.inf.tu-dresden.de/literatur/Anon_Terminology_v0.34.pdf
.. [Batching] https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.941.6881&rep=rep1&type=pdf,
.. [BibMixnets] https://bib.mixnetworks.org/
.. [Byzantine] https://people.csail.mit.edu/idish/ftp/Brahms-PODC.pdf
.. [DesignAnonymous] https://www.freehaven.net/anonbib/cache/wiangsripanawan-acsw07.pdf
.. [Gossip] http://pages.di.unipi.it/ricci/GossipBasedPeerSampling.pdf
.. [Loopix] https://arxiv.org/pdf/1703.00536.pdf
.. [MixCascadesP2P] https://www.esat.kuleuven.be/cosic/publications/article-523.pdf
.. [Mixmaster] https://tools.ietf.org/html/draft-sassaman-mixmaster-03
.. [MixminionPaper] https://www.mixminion.net/minion-design.pdf
.. [Sphinx] https://bib.mixnetworks.org/pdf/DBLP:conf/sp/DanezisG09.pdf
.. [Stopandgo] https://www.freehaven.net/anonbib/cache/stop-and-go.pdf
.. [SurveyRouting] https://dl.acm.org/doi/10.1145/3182658
.. [Trickle] https://www.freehaven.net/anonbib/cache/trickle02.pdf
.. [Trilemma] https://doi.org/10.1109/SP.2018.00011
.. [Uniform] www.irisa.fr/dionysos/pages_perso/sericola/PAPIERS/DSN13.pdf
.. [Untraceable] http://www.ovmj.org/GNUnet/papers/p84-chaum.pdf

Web pages
---------

.. [AnonymousRemailer] https://en.wikipedia.org/wiki/Anonymous_remailer
.. [ApplicationThreatModeling] https://owasp.org/www-community/Application_Threat_Modeling
.. [CERT] https://git.gnunet.org/gnunet.git/tree/src/gnsrecord/plugin_gnsrecord_dns.c#n130
.. [DHT] https://en.wikipedia.org/wiki/Distributed_hash_table
.. [gnunetGNS] https://en.wikipedia.org/wiki/GNUnet#GNU_Name_System
.. [gnunetGNUnet] https://en.wikipedia.org/wiki/GNUnet
.. [HuMixmaster] https://sarwiki.informatik.hu-berlin.de/Mixmaster_Remailer
.. [katzenpostSite] https://katzenpost.mixnetworks.org/
.. [katzenpostSURB] https://github.com/katzenpost/docs/blob/subscription_plugin.0/drafts/mix_plugin_subscription.rst
.. [Mailproxy] https://github.com/katzenpost/mailproxy
.. [MAPI] https://en.wikipedia.org/wiki/MAPI
.. [Mixmailer] https://gitea.pep.foundation/pEp.foundation/mixmailer
.. [MixmailerSlides] https://gitea.pep.foundation/pEp.foundation/mixmailer_slides
.. [Mixminion] https://en.wikipedia.org/wiki/Mixminion
.. [MixNetworks] https://en.wikipedia.org/wiki/Mix_network
.. [MixOnion] https://ritter.vg/blog-cryptodotis-mix_and_onion_networks.html
.. [NymServer] https://en.wikipedia.org/wiki/Pseudonymous_remailer#Contemporary_nym_servers
.. [Nymtech] https://nymtech.net/
.. [OnionRouting] https://en.wikipedia.org/wiki/Onion_routing
.. [PET] https://en.wikipedia.org/wiki/Privacy-enhancing_technologies
.. [RemailerNym] https://en.wikipedia.org/wiki/Pseudonymous_remailer
.. [SPF] https://en.wikipedia.org/wiki/Sender_Policy_Framework
.. [STRIDE] https://en.wikipedia.org/wiki/STRIDE
.. [ThreatModelingOutputs] https://owasp.org/www-community/Threat_Modeling_Outputs
.. [TypeI] https://en.wikipedia.org/wiki/Anonymous_remailer#Cypherpunk_remailers,_also_called_Type_I
.. [TypeII] https://en.wikipedia.org/w/index.php?title=Anonymous_remailer&section=4#Mixmaster_remailers,_also_called_Type_II
.. [TypeIII] https://en.wikipedia.org/wiki/Anonymous_remailer#Mixminion_remailers,_also_called_Type_III



Other
-----

.. [AttacksAnonymitySystems] https://www.blackhat.com/presentations/bh-usa-03/bh-us-03-sassaman-dingledine/bh-us-03-dingledine.pdf, https://www.blackhat.com/presentations/bh-usa-03/bh-us-03-sassaman-dingledine/bh-us-03-sassaman.pdf, https://www.youtube.com/watch?v=RQ1ikYB_1LY, https://www.youtube.com/watch?v=tAuCX9V034Q
