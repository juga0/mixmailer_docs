.. _concepts:

Concepts
========

Anonymity
---------

What is anonymity?

In the context of Privacy Enhancing Technologies (PET)  [PET]_, what
people usually mean by anonymity is **unlinkability**, which is still
vague cause does not specify what with respect to who is unlinkable.

More specific terms related unlinkability, ie. who is talking to who
with respect some adversaries with certain capabilities  [AnonTerms]_.

- **sender anonymity**
- **receiver anonymity**
- **location anonymity**
- **third party anonymity**

Even more specific:

- **sender unobservability** : whether the sender is talking at all
- **receiver unobservability**

To be able to define the desired “properties” in “anonymous”
communication systems, threat models should be specified.

.. _concepts_adversaries:

Adversaries
-----------

Passive adversary
~~~~~~~~~~~~~~~~~

Adversary observing both ends
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Can link sender and receiver by timing and volume patterns

Global Passive adversary
~~~~~~~~~~~~~~~~~~~~~~~~

Active adversary
~~~~~~~~~~~~~~~~

.. _adversary-observing-both-ends-1:

Adversary observing both ends
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Confirmation attacks: Adversary can link sender and receiver by inducing
timing signatures on the traffic to force distinct patters

Rogue operators
~~~~~~~~~~~~~~~

Malicious node operators. Passive or active.

.. _concepts_threat_model:

Threat models
-------------

Which adversaries a system protect or does not protect against?

For instance, in Wikipedia edits:

- Sysadmins can link one user unregistered edit to another by the IP
- If editing in a company, the company can see the amount of data at a
   certain time which can match an be seen the public Wikipedia edit.

The following is based on [ApplicationThreatModeling]_ and
[ThreatModelingOutputs]_

Process
~~~~~~~

What are we building?
^^^^^^^^^^^^^^^^^^^^^

- architecture diagrams
- dataflow transitions
- data classifications

What can go wrong?
^^^^^^^^^^^^^^^^^^

[STRIDE]_ and other structures can help.

Outputs
~~~~~~~

Template
^^^^^^^^

Some data flow (eg. SMTP transmission of message from N1 to N2)
Some data (eg. message being transmitted)
Threat (eg. headers not encrypted, information disclosure), status (eg. open),
severity (eg. high)
Mitigation (eg. link encryption with TLS)
