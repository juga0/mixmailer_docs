.. _pep-msgs:

pEp mesages structure
=====================

pEp messages structure when encrypting/decrypting several layers.

Example in which Alice encrypts first a message for Bob, then encrypts
the encrypted message to Carol. Alice sends the message to Carol. Carol
receives de message, decrypt, and sends the body of the decrypted
message to Bob. Bob receives, decrypt it and see the message that Alice
sent. The communication happens then in this direction:

Alice -> Carol -> Bob

Alice wants to send this message to Bob
---------------------------------------

::

   Hi Bob,
   this is Alice!

Alice creates the message for Bob
---------------------------------

::

   MIME-Version: 1.0
   Content-Type: multipart/mixed; boundary="4bc216fb60fb5f2d6a48d71f5810ff9e"

   --4bc216fb60fb5f2d6a48d71f5810ff9e
   Content-Type: text/plain; charset="utf-8"
   Content-Transfer-Encoding: quoted-printable
   Content-Disposition: inline; filename="msg.txt"

   Subject: Subject: from Alice to Bob

   Hi Bob,
   this is Alice!

   --4bc216fb60fb5f2d6a48d71f5810ff9e
   Content-Type: application/pgp-keys
   Content-Disposition: attachment; filename="pEpkey.asc"

   -----BEGIN PGP PUBLIC KEY BLOCK-----
   [...]
   -----END PGP PUBLIC KEY BLOCK-----

   --4bc216fb60fb5f2d6a48d71f5810ff9e--

Alice encrypts the message for Bob
----------------------------------

::

   From: Alice Lovelace <alice@openpgp.example>
   To: Bob Babagge <bob@openpgp.example>
   Subject: =?utf-8?Q?p=E2=89=A1p?=
   X-pEp-Version: 2.1
   MIME-Version: 1.0
   Content-Type: multipart/encrypted; boundary="2cfc2f5be8c232219b7b77f1a79092a";
   protocol="application/pgp-encrypted"

   --2cfc2f5be8c232219b7b77f1a79092a
   Content-Type: application/pgp-encrypted

   Version: 1
   --2cfc2f5be8c232219b7b77f1a79092a
   Content-Type: application/octet-stream
   Content-Transfer-Encoding: 7bit
   Content-Disposition: inline; filename="msg.asc"

   -----BEGIN PGP MESSAGE-----
   [...]
   -----END PGP MESSAGE-----

   --2cfc2f5be8c232219b7b77f1a79092a--

Alice creates a message for Carol
---------------------------------

Note that the message headers from Alice to Bob are included in the body
of the message from Alice to Carol.

::

   From: Alice Lovelace <alice@openpgp.example>
   To: Carol Hopper <carol@openpgp.example>
   Subject: Subject: from Alice to Bob
   MIME-Version: 1.0
   Content-Type: text/plain; charset="utf-8"
   Content-Transfer-Encoding: quoted-printable
   Content-Disposition: inline; filename="msg.txt"

   =46rom: Alice Lovelace <alice=40openpgp.example>
   To: Bob Babagge <bob=40openpgp.example>
   Subject: =3D=3Futf-8=3FQ=3Fp=3DE2=3D89=3DA1p=3F=3D
   X-pEp-Version: 2.1
   MIME-Version: 1.0
   Content-Type: multipart/encrypted; boundary=3D=2256788e955154139fffab44e5=
   f290f91=22; =20
   protocol=3D=22application/pgp-encrypted=22

   --56788e955154139fffab44e5f290f91
   Content-Type: application/pgp-encrypted

   Version: 1
   --56788e955154139fffab44e5f290f91
   Content-Type: application/octet-stream
   Content-Transfer-Encoding: 7bit
   Content-Disposition: inline; filename=3D=22msg.asc=22

   -----BEGIN PGP MESSAGE-----
   [...]
   -----END PGP MESSAGE-----

   --56788e955154139fffab44e5f290f91--

Alice encrypts the message for Carol
------------------------------------

::

   From: Alice Lovelace <alice@openpgp.example>
   To: Carol Hopper <carol@openpgp.example>
   Subject: =?utf-8?Q?p=E2=89=A1p?=
   X-pEp-Version: 2.1
   MIME-Version: 1.0
   Content-Type: multipart/encrypted; boundary="6fa56ec33679af179f5f59a10722e17";
   protocol="application/pgp-encrypted"

   --6fa56ec33679af179f5f59a10722e17
   Content-Type: application/pgp-encrypted

   Version: 1
   --6fa56ec33679af179f5f59a10722e17
   Content-Type: application/octet-stream
   Content-Transfer-Encoding: 7bit
   Content-Disposition: inline; filename="msg.asc"

   -----BEGIN PGP MESSAGE-----
   [...]
   -----END PGP MESSAGE-----

   --6fa56ec33679af179f5f59a10722e17--

and sends it to Carol

Carol receives the message and decrypts it
------------------------------------------

::

   From: Alice Lovelace <alice@openpgp.example>
   To: Carol Hopper <carol@openpgp.example>
   Subject: Subject: from Alice to Bob
   X-pEp-Version: 2.1
   X-EncStatus: reliable
   X-KeyList:
   EB85BB5FA33A75E15E944E63F231550C4F47E38E,EB85BB5FA33A75E15E944E63F231550C4F47E38E,37D13DE1DCBAFCE7BCE117EE311399EB28E3E8AA
   MIME-Version: 1.0
   Content-Type: text/plain; charset="utf-8"
   Content-Transfer-Encoding: quoted-printable
   Content-Disposition: inline; filename="msg.txt"

   =46rom: Alice Lovelace <alice=40openpgp.example>
   To: Bob Babagge <bob=40openpgp.example>
   Subject: =3D=3Futf-8=3FQ=3Fp=3DE2=3D89=3DA1p=3F=3D
   X-pEp-Version: 2.1
   MIME-Version: 1.0
   Content-Type: multipart/encrypted; boundary=3D=225a2e1b238a9435439d7f32eb=
   295b6f=22;  =20
   protocol=3D=22application/pgp-encrypted=22

   --5a2e1b238a9435439d7f32eb295b6f
   Content-Type: application/pgp-encrypted

   Version: 1
   --5a2e1b238a9435439d7f32eb295b6f
   Content-Type: application/octet-stream
   Content-Transfer-Encoding: 7bit
   Content-Disposition: inline; filename=3D=22msg.asc=22

   -----BEGIN PGP MESSAGE-----
   [...]
   -----END PGP MESSAGE-----

   --5a2e1b238a9435439d7f32eb295b6f--

Carol sends the body of the message to Bob

Bob receives and decrypt it
---------------------------

::

   From: Alice Lovelace <alice@openpgp.example>
   To: Bob Babagge <bob@openpgp.example>
   Subject: Subject: from Alice to Bob
   X-pEp-Version: 2.1
   X-EncStatus: reliable
   X-KeyList:
   EB85BB5FA33A75E15E944E63F231550C4F47E38E,EB85BB5FA33A75E15E944E63F231550C4F47E38E,D1A66E1A23B182C9980F788CFBFCC82A015E7330
   MIME-Version: 1.0
   Content-Type: text/plain; charset="utf-8"
   Content-Transfer-Encoding: quoted-printable
   Content-Disposition: inline; filename="msg.txt"

   Hi Bob,
   this is Alice=21
