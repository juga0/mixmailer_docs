.. _mix-networks:

Mix networks
============

Mix networks systems and software considered to be used for the pEp
MixMailer.

   I had used the terms "mix network" and "onion routing" almost
   interchangeably. In actuality I had fallen into a trap that a fair
   number of people familiar with the space have fallen into: using
   those terms without a solid differentiation. This blog post aims to
   correct that.

 [MixOnion]_

   Mix networks are routing protocols that create hard-to-trace
   communications by using a chain of proxy servers known as mixes which
   take in messages from multiple senders, shuffle them, and send them
   back out in random order to the next destination (possibly another
   mix node). This breaks the link between the source of the request and
   the destination, making it harder for eavesdroppers to trace
   end-to-end communications. Furthermore, mixes only know the node that
   it immediately received the message from, and the immediate
   destination to send the shuffled messages to, making the network
   resistant to malicious mix nodes.

   Each message is encrypted to each proxy using public key
   cryptography; the resulting encryption is layered like a Russian doll
   (except that each "doll" is of the same size) with the message as the
   innermost layer. Each proxy server strips off its own layer of
   encryption to reveal where to send the message next. If all but one
   of the proxy servers are compromised by the tracer, untraceability
   can still be achieved against some weaker adversaries.

 [MixNetworks]_

.. image:: /_static/Red_de_mezcla.png
   :alt: image

.. _pseudonomous-remailers:

Pseudonymous remailers
----------------------

   A pseudonymous remailer or nym server, as opposed to an anonymous
   remailer, is an Internet software program designed to allow people to
   write pseudonymous messages on Usenet newsgroups and send
   pseudonymous email. Unlike purely anonymous remailers, it assigns its
   users a user name, and it keeps a database of instructions on how to
   return messages to the real user. These instructions usually involve
   the anonymous remailer network itself, thus protecting the true
   identity of the user.

 [RemailerNym]_

.. _nym-servers:

Nym servers
~~~~~~~~~~~

   A nym server (short for "pseudonym server") is a server that provides
   an untraceable e-mail address, such that neither the nym server
   operator nor the operators of the remailers involved can discover
   which nym corresponds to which real identity.

   To set up a nym, you create a PGP keypair and submit it to the nym
   server, along with instructions (called a reply block) to anonymous
   remailers (such as Cypherpunk or Mixmaster) on how to send a message
   to your real address. The nym server returns a confirmation through
   this reply block. You then send a message to the address in the
   confirmation.

 [NymServer]_

.. _anonymous-remailers:

Anonymous remailers
-------------------

   An anonymous remailer is a server that receives messages with
   embedded instructions on where to send them next, and that forwards
   them without revealing where they originally came from. There are
   Cypherpunk anonymous remailers, Mixmaster anonymous remailers, and
   nym servers, among others, which differ in how they work, in the
   policies they adopt, and in the type of attack on anonymity of e-mail
   they can (or are intended to) resist

 [AnonymousRemailer]_

.. _type-i-cypherpunks:

Type I (Cypherpunks)
~~~~~~~~~~~~~~~~~~~~

   A Cypherpunk remailer sends the message to the recipient, stripping
   away the sender address on it. One can not answer a message sent via
   a Cypherpunk remailer. The message sent to the remailer can usually
   be encrypted, and the remailer will decrypt it and send it to the
   recipient address hidden inside the encrypted message. In addition,
   it is possible to chain two or three remailers, so that each remailer
   can't know who is sending a message to whom. Cypherpunk remailers do
   not keep logs of transactions.

 [TypeI]_

More documentation on Cypherpunk remailers:

https://www.iusmentis.com/technology/remailers/index-cpunk.html

https://www.iusmentis.com/technology/remailers/index-anon.html

   Last modified: 20 Jun 1998

Properties:

- Transport (between the Cypherpunk remailers): SMTP / POP3
- Format: MIME?

Cons:

- replay attacks
- flooding attacks
- no central directory server, which creates an asymmetry of
   information about the remailers in the network. The not equally
   distributed knowledge of current servers can be exploited to
   undermine anonymity and is one of the Cons of the system

 [HuMixmaster]_

- no user friendly clients
- old crypto
- no possibility to replay
- no network diversity/poor anonymity set
- spam

.. image:: /_static/Figure1.gif
   :alt: image

.. _type-ii-mixmaster:

Type II Mixmaster
~~~~~~~~~~~~~~~~~

   In Mixmaster, the user composes an email to a remailer, which is
   relayed through each node in the network using SMTP, until it finally
   arrives at the final recipient. Mixmaster can only send emails one
   way. An email is sent anonymously to an individual, but for them to
   be able to respond, a reply address must be included in the body of
   the email. Also, Mixmaster remailers require the use of a computer
   program to write messages. Such programs are not supplied as a
   standard part of most operating systems or mail management systems.

 [TypeII]_

Mixmaster Protocol Version 2, December 29, 2004:

Cryptographic Algorithms:

   The asymmetric encryption mechanism is RSA with 1024 bit RSA keys and
   the PKCS #[MixOnion] v1.5 (RSAES-PKCS1-v1_5) padding format [Nymtech]. The
   symmetric encryption mechanism is EDE 3DES with cipher block chaining
   (24-byte key, 8-byte initialization vector). MD5 is used as
   the message digest algorithm.

Packet format:

   The header sections (except for the first one) and the packet body
   are encrypted with symmetric session keys

 [Mixmaster]_

Properties:

- topology: cascade (no scalability, then poor anonymity set)
- mixing strategy: batch and reorder (unpredictable delays) Have an
   adjustable size message pool in which incoming packets are collected
   and re-sorted randomly before being forwarded.
- Key/node discovery: pinger
- Transport: SMTP
- Format: MIME?
- Packets: all packets are the same size.
- Packet format:
- Cover traffic: can send dummy packages
- Compatible with Cypherpunk messages.

Pros:

- third party untraceability between sender and receiver
- resistant to timing correlation attacks
- support replies or anonymous recipients (not resistant to replay
   attacks)

Cons:

- not resistant to active attacks, eg. dropping packets and causing
  denial of service
- not resistant to replay attacks
- no central directory server (network partitioning)
- no user friendly clients
- old crypto
- no replies
- no network diversity/poor anonymity set
- spam

Packet structure:

.. image:: /_static/Mixmaster_Packetaufbau.png
   :alt: image

.. image:: /_static/Figure2.gif
   :alt: image

.. _type-iii-mixminion:

Type III Mixminion
~~~~~~~~~~~~~~~~~~

   A Mixminion remailer attempts to address the following challenges in
   Mixmaster remailers: replies, forward anonymity, replay prevention
   and key rotation, exit policies, integrated directory servers and
   dummy traffic. They are currently available for the Linux and Windows
   platforms. Some implementations are open source.

 [TypeIII]_

   Mixminion supports Single-Use Reply Blocks (or SURBs) to allow
   anonymous recipients. A SURB encodes a half-path to a recipient, so
   that each mix in the sequence can unwrap a single layer of the path,
   and encrypt the message for the recipient. When the message reaches
   the recipient, the recipient can decode the message and learn which
   SURB was used to send it; the sender does not know which recipient
   has received the anonymous message.

 [Mixminion]_

Properties:

- Transport: TLS instead of SMTP.
- Key/node discovery: directory servers or pinger
- Key rotation:
- Up to 32 instead of 20 hops.
- Packet format: Sphinx
- Compatible with Cypherpunk and Mixmaster messages.

Pros:

- directory servers (no partitioning)
- Indistinguishable between forward and reply messages.
- Forward secrecy by using TLS

Cons:

- no user friendly clients
- old crypto
- no network diversity
- spam

.. image:: /_static/Struktursmall.jpg
   :alt: image

Remailer vulnerabilities
------------------------

https://www.freehaven.net/anonbib/cache/rprocess.html

.. _katzenpost:

Katzenpost
----------

Properties:

- topology: stratified (scales, better anonymity set)
- mixing strategy: stop and go (predictable relays)
- packet format: Sphinx
- it has Providers as entry point that require authentication,
   therefore it is needed to first
- sender needs to know the receiver Provider (no receiver anonymity?)

Pros:

- third party untraceability between sender and receiver
- better scalability, therefore better network diversity/anonymity set
- predictable delays
- it could be possible to use the mixnet to send Email only between two
   organizations that “trust” each other without SPF to have 3rd party
   unlinkability

Cons:

- License is AGPL v3, not compatible with xGPL v2, which means it can’t
   be part of iOS. katzenpost license can not be changed cause one of
   their authors. We’d need to rewrite katzenpost client and mailproxy
   to use it in iOS
- not designed to run with Email messages there is a mailproxy, but
   katzenpost devs are unhappy with the mail proxy
- no production network atm
- sender needs to authenticate with a Provider
- sender needs to know the receiver Provider (no sender anonymity)
- for the providers to be able to send Email to any destination, they
   need to use SPF, therefore no sender anonymity
- different Email traffic patterns (eg. Deltachat) would need different
   delay parameters

 [katzenpostSite]_

.. _nym-mixnet:

Nym mixnet
----------

Cons:

- License is Apache, not compatible with GPL. So far all software
   created by pEp must be GPL(-compatible).

 [Nymtech]_
