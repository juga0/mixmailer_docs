Code API / Interface
====================

Proposed by V.

Node Interface to register as a node
------------------------------------

::

   PEP_STATUS register_node(PEP_SESSION session, pEp_identity *ident);

::

   def register_node(ident):
       "parameter ident must be a pEp.Identity"

Client Interface to discover nodes
----------------------------------

::

   PEP_STATUS get_registered_nodes(PEP_SESSION session, identity_list **nodes);

::

   def get_registered_nodes():
      "returns list with identities"
