.. _proposals_arguments:

Proposals 1, 2 and 1.1 arguments
================================

See :ref:`p1`, :ref:`p2` and :ref:`p11`.

Pro and con arguments for the different proposals discussed after juga's
presentation on January 12, 2021 [MixmailerSlides]_ .

Stratified topology
-------------------

V. arguments against:

- attacks on the exits
- authorities decide the possition
- nodes should decide the route (free-routing)

j counterarguments:

- attacks on exits:

 - do not deanonymize sender, nor their location, per se
 - can be done by any operator that runs the exit:

   - reason why TLS is recommended (avoid MiTM)

 - if target is the receiver, it is easy to find the "random" exit

- intelligence agencies try more sophisticated attacks trying to deanonymize
  the whole path
- it's the node that decide the possition in Loopix

Authorities
-----------

j arguments:

- they're needed so that:

  - all clients have the same view of the network, otherwise sybil attack
  - can reward/penalize nodes that go on and off, missbehave, etc.

V. counterarguments:

- it's the nodes which take those decissions.

TLS
---

j arguments:

- avoid clear metadata at last hop
- Let's Encrypt has helped a lot to do not depend on 3rd party entities

V. counterarguments:

- 0 trust on TLS

Message size
------------

j arguments:

- can't pad while not possible to predict final size, what is only possible
  with same OpenPGP algo. and without compression
- need of fixed size (huge) padding so that the attacker doesn't know in which
  position of the route is the message

V. counterarguments:

- random padding, if message is too big and it's know it's at 1st possition in
  path, bad luck

OpenPGP
-------

V.: pEp is not OpenPGP/MIME when 2 pEp clients talk, but it's OpenPGP/MIME
compatible when the receiver is not pEp client.

Hidden headers
--------------

V.:

- agrees that the metadata is clear at last hop
- Outside (clear) `From` might not be the same as the inside (encrypted) `From`
- it has been implemented since years

GNS
---

V.: it's needed that GNUnet implements a GNS library

Other
-----

V.: all this should be impemented in the engine including GNS
resolving/registering


Katzenpost
----------

j arguments:

- we can rewrite mail proxy. License of nodes running software doesn't matter cause
  pEp is not going to run them

V. contrarguments: not the technical solution we want
