“Open” vs “close” system
========================

In both :ref:`p1` and :ref:`p2` we should decide whether
it should be possible to send Email to any Email service, eg. gmail or
to a few Email services, eg. riseup and systemli or several different
companies.

In the “open” system there’s no solution to avoid spam. If the public
keys are available and anyone can join the mixnet, spammers can also
send encrypted spam. Also, services like Gmail will refuse to receive
Email from other domains/services not using  [SPF]_. If the mixnet
provider/service uses SPF, then it’s not possible to have sender
anonymity (unlinkability).

In the “close” system, a malicious provider could also inject spam, but
it’d be less likely, specially if the users have to authenticate to the
providers, which is the case of :ref:`katzenpost` .

Diagram with errors:

.. figure:: /_static/openvsclosesystem.svg
   :alt: image

   image
