Here specification.
It is in markdown so that kramdown can convert it to xml for RFCs.
It is recommended to read [Proposal1.1](../Design/Proposal1.1) and
[Threat model](../Design/threat_model).

Introduction
============

TBD

Requirements Language
---------------------

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD",
"SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED", "MAY", and "OPTIONAL" in this
document are to be interpreted as described in BCP 14 ([RFC2119] and [RFC8174])
when, and only when, they appear in all capitals, as shown here.

Terminology
-----------

TBD

Own vs existing.

Mix network/mixnet

Onion routing

Remailer

Node

Mix

client

pEp engine

GNUnet node

GNS Resolution

GNS Registration

GNS Delegation

Message

All the content
===============

TBD

Security Considerations
=======================

TBD
