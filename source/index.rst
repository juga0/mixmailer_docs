.. pEp MixMailer documentation master file, created by
   sphinx-quickstart on Mon Feb 24 06:45:18 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pEp MixMailer's documentation!
=============================================

Analysis
--------

.. toctree::
   :maxdepth: 1
   :glob:

   Analysis/*

Design
------

.. toctree::
   :maxdepth: 1
   :glob:

   Design/*

Specification
-------------

.. toctree::
   :maxdepth: 1
   :glob:

   Specification/*

Roadmap
-------

.. toctree::
   :maxdepth: 1

   roadmap

References
------------

.. toctree::
   :maxdepth: 1

   references


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
