.. _mix-networks-sw:

Mix networks software
=====================

reliable
--------

Remailers Type I and II users’ maunal :

https://gateway.ipfs.io/ipfs/QmboFHizh9ys57DXcVsniVDYS46gsiBP716u2sqQE7xgV4

https://www.panta-rhei.dyndns.org/JBNR-en.htm#CForm

   Revised: 23-Sep-99 [Reliable v1.0.2]

Are the OpenPGP encrypted emails not padded?

mixmaster
---------

Code in sourceforge [^mixmaster origin] is not in CVS system.

Uses RSA (and DSA?) for encryption and PKCS#1 for padding.

Several repositories with some updates

https://github.com/eurovibes/mixmaster:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   Mixmaster version 3 – anonymous remailer software – (C) 1999
   Anonymizer Inc.

Commits from Nov 2001 to Oct 2001?

One of the first versions, 2.9beta32 says:

   It supports OpenPGP encryption (compatible with PGP 2, PGP 5 and
   GnuPG).

(Only for the recipient or also for the mixes?)

https://github.com/crooks/mixmaster
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Commits from 2004 (including previous code) to 2014

https://github.com/merkinmuffley/mixmaster4096
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Commits from 2014 to 2018

Encryption with 4096 bits RSA

https://github.com/crooks/pymaster
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Mixmaster version 3 implementation in Python

Commits from Feb 2013 to Jun 2013.

Uses RSA for encryption and PKCS#1 for padding.

OpenPGP is used only for the remailer admins.

No license

https://github.com/crooks/mimix
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   provide a Mixmaster-like protocol with support for larger RSA keys
   (up to 4096 bits), replacement Hash (SHA2) and Symmetric Ciphers
   (AES). Instead of using SMTP for inter-Remailer traffic, Mimix uses
   HTTP.

Commits from Oct 2013 to Mar 2014

GPL license

Mixminion
---------

Commit from 2002 to 2012.

C bindings and Python

License until v1.0.0 GPL, later BSD

https://github.com/tim54000/cypherpunk-cli
------------------------------------------

Rust

2019

https://github.com/byte-mug/go-cypherpunk
-----------------------------------------

Go

Aug 2020

Running remailers
-----------------

https://apricot.fruiti.org/echolot/rlist2.html

Last update: Thu Sep 3 10:30:01 2020.

All OpenPGP remailer keys are dsa1024 for signing and elg1024 for
encryption, except for remailer@kroken.de.eu.org, which are rsa4096.

::

   $remailer{"austria"} = "<mixmaster@remailer.privacy.at> cpunk max mix pgp pgponly repgp remix latent hash cut test ekx inflt50 rhop5 reord klen1024";
   $remailer{"cloaked"} = "<mixmaster@cloaked.pw> cpunk max mix middle pgp pgponly repgp remix esubbf hsub latent hash cut test ekx inflt50 rhop5 reord post klen1024";
   $remailer{"dizum"} = "<remailer@dizum.com> cpunk max mix pgp pgponly repgp remix latent hash cut test ek ekx esub inflt50 rhop5 reord post klen64";
   $remailer{"frell"} = "<godot@remailer.frell.eu.org> cpunk max mix pgp pgponly repgp remix latent hash cut test ekx inflt50 rhop5 reord post klen1024";
   $remailer{"hsub"} = "<hsub@mixmaster.mixmin.net> cpunk max pgp pgponly repgp remix esubbf hsub latent hash cut test ekx inflt50 rhop5 reord klen100";
   $remailer{"kroken"} = "<remailer@kroken.de.eu.org> cpunk max mix middle pgp pgponly repgp remix latent hash cut test ekx inflt50 rhop5 reord klen1024";
   $remailer{"lambton"} = "<remailer@lambton.org> cpunk max mix middle pgp pgponly repgp remix esubbf hsub latent hash cut test ekx inflt50 rhop5 reord klen256";
   $remailer{"paranoia"} = "<mixmaster@remailer.paranoici.org> cpunk max mix pgp pgponly repgp remix latent hash cut test ekx inflt50 rhop5 reord post klen150";
   $remailer{"redjohn"} = "<remailer@redjohn.net> cpunk max mix middle pgp repgp remix esub hsub latent hash cut test ek ekx inflt50 rhop5 reord post klen1024";
   $remailer{"senshi"} = "<senshiremailer@gmx.de> cpunk middle pgp latent ek ekx esub cut hash repgp reord ext max test inflt10 rhop2 klen200";
   $remailer{"zip2"} = "<mix@mail.zip2.in> cpunk max mix pgp pgponly repgp remix esubbf hsub latent hash cut test ekx inflt50 rhop5 reord post klen1024";

Katzenpost
----------

See :ref:`katzenpost`

Nym Mixnet
----------

See :ref:`nym-mixnet`
