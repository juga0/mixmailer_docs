Requirements
============

TBD

Without defining the requirements first, any software we write will have
to be re-written many times.

What to implement
-----------------

.. raw:: html

   <s>pEp engine transport for messages</s>

::

    Add mixnet capabilities to SMTP transport

What is it for? (updated Aug 18, 2020)
--------------------------------------

- to hide metadata, in this case, Email headers YES Email from the last
  node to the receiver will have the headers that pEp
  adds/removes/modifies

  - to a third party observer? (unlinkability, possible correlation
    attacks) YES
  - to the receiver? (sender anonymity) NO

    - what about the sender Email ``From:`` header when the receiver
      wish to reply to the sender?
    - what about the sender Email ``Received:`` header?, that might
      disclose sender geographic location?
    - what about the sender Email ``Date:`` header?, which discloses
      the time when the Email was sent
    - untraceable return address? (receiver can reply but doesn’t
      know the location of the sender?)
  - to the sender? (receiver anonymity): NO the sender does not know
    which recipient has received the message.
- to be compatible with existing Mixmaster network? Mixmaster network
  still works, but small anonymity set, so NO
- to reuse the pieces that pEp or partners have developed? CAN
- to not to reinvent the wheel? SHOULD
- to have a system that anyone can extend? (scalability)

  - how to convince others to run nodes? > by the extra capabilities
    pEp offers > intial nodes can be run by friends

Who are users?
--------------

who is going to use MixMailer?

- pEp clients? YEA
- anyone? NO

estimated number of users/clients?::

    pEp scales to billions of users

Who is going to operate the/maintain MixMailer?
------------------------------------------------

- pEp company/foundation? NO
- pEp partners? NO
- anyone? YES
- estimated number of nodes?

Usability
---------

It should be a solution that is usable by the “end user”, not only by
the nerd::

   transparent to the user

Which would be the initial components of the system?
----------------------------------------------------

- pEp client (frontend), eg. Thunderbird plugin::

    pEp client is part of the engine

- pEp engine (backend), would route the Email to MixMailer
