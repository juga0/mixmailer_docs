.. _onion-routing:

Onion routing
=============

   Onion routing is a technique for anonymous communication over a
   computer network. In an onion network, messages are encapsulated in
   layers of encryption, analogous to layers of an onion. The encrypted
   data is transmitted through a series of network nodes called onion
   routers, each of which "peels" away a single layer, uncovering the
   data's next destination. When the final layer is decrypted, the
   message arrives at its destination. The sender remains anonymous
   because each intermediary knows only the location of the immediately
   preceding and following nodes

 [OnionRouting]_

.. figure:: /_static/Onion_diagram.svg
   :alt: image
