.. _p1:

Proposal 1 (P1) design
======================

aka pEp onion routing or mix network or remailer?

TBD.

Based on notes talking with V. on February 3, 2020 and in March.

See :ref:`comparative` for an overview about onion routing and mix networks.

Routing
-------

Client decide the route for the message. > The client encrypts the
message with the recipients OpenPGP key and some nodes’ OpenPGP keys.

See :ref:`pep-msgs`.

This in a similar to :ref:`type-i-cypherpunks` remailers.

   The client sends the message to the first node. The first node
   receive the message and decrypt it. It can know whether the message
   is from a trusted node (by the color).

(How the color is calculated?, by the signature of the sender?)

Since the message is already encrypted for the next node, there is no
need to re-encrypt it.

(Does the node needs to sign it before sending it to the next node?)

Transport
---------

   SMTP. No plan to enforce TLS cause Sequoia is working on Forward
   Secrecy.

(How would forward secrecy work with different layers of encryption?)

Nodes’ keys
-----------

   It would be the same type of key that the engine is using. If engine
   is using Sequoia, the keys will be OpenPGP. It could be symetric
   keys, but for onion routing they need to be asymmetric.

(Not really true. Tor generates a symetric session key for circuit to
minimize the expensive computation with asymetric keys)

Key rotation
------------

   No automatic key rotation. The engine do key distribution and key
   reset Key reset can happen actively or passively.

(In Katzenpost key rotation happen often and the key rotation moment is
vulnerable to attacks. OpenPGP keys don’t change that often.)

Key discovery (by clients)
--------------------------

   The first time that a user/node send a message, the key is attached.
   A client needs to exchange a couple of messages with every node to
   discover their keys.

(How would this scale with thousands of nodes?)

Nodes and key discovery by clients and nodes
--------------------------------------------

Via :ref:`gns`

   Nodes should register, with an email address and a key, what is
   already in pEp identity. The key should be in ASCII armor Clients
   obtain the nodes from GNS.

(did V. mention Bézier curve?)

CG proposed to use the CERT record type  [CERT]_

.. _routing-1:

Routing
-------

   First client obtain the nodes. Then client choose the route It is
   still needed to define the algorithm to choose the route. Initially
   it could be a dummy algorithm, like choose 3 nodes randomly. There
   should be a maximum of hops to choose.

Even choosing always the same route path, some degree of anonymity can
be achieved by mixing packets of the same size and introducing delays.

Notes on Aug 12, 2020:

   In the context of Mixnets “free routing” gives actually less
   anonymity since there’s less entropy, so it’s actually better to
   implement cascade topology (fixed position in the route). With this
   topology as long as 1 node is honest, it’s fine. It’s even better, to
   have an stratified topology, in which there’re groups of nodes for
   then entry, the middle and the exit. The problem of free routing is
   that eventually you might end up choosen a path where all the nodes
   are compromised. In Tor this is minimized by having guard (sticky
   entry) nodes.

Types of nodes
--------------

   All the same. This avoid the exit node problems.

(which exit node problems avoid this?, spam?)

Packet format
-------------

   Messages are MIME Multipart Encrypted (and Signed?) So far messages
   are not divided in packets nor padded. The message encrypted for all
   layers would need to be divided into packets. This is still not
   implemented. Clients might need that a message is a packet for
   debugging.

Packet size
-----------

   So far packets would have the size that the underlying transport give
   it to them. Once the engine divide messages in packets, they can be
   padded. The engine is already capable to pad messages.

(packets need to be all the same size -and mixed- to achieve
unlinkability)

Notes on Aug 12, 2020:

   Impl. fixed-sized padding to messages. We could have 2 different
   packet/message size. One “large” and one “small”. eg. 50mb and 50kb)

OpenPGP already does padding and we would need to modify that operation,
but we should not modify the way OpenPGP works.

Only remailers Type I use OpenPGP for encryption.

Packet mixing
-------------

   There would be mixing of packets once they’re the same size.

(If there is no packet mixing, it won’t be a mix network nor a
remailer.)

Cover traffic
-------------

   Not planned

Notes on Aug 12, 2020:

   Cover traffic mitigates statistical disclosal attack/analysis, which
   adds the “unobservability” property.

Delays
------

We have not discussed this.

Notes on Aug 12, 2020:

   Delay and (one of the strategies, mixmaster’s one, is “pooling”)
   (could use postfix queue), which add the “unlinkability” property

Directory server
----------------

   There are not directory servers. Trusted nodes using trustwords.

(How this would work?)

Reply
-----

   Since the recipient sees who was the original sender, there is no
   need for special type of reply.

(Then there won’t be sender anonymity from the the recipient point of
view)

Spam
----

   Since the Emails are encrypted, there won’t be spam.

Remailers require to have the node key to send an Email to them, that
did not prevent spam.

(How would spam be prevented using discoverable OpenPGP keys?, how would
the Emails arrive to recipients without SPF or DKIM?, how there could be
sender anonymity with SPF?)

Network diversity
-----------------

   Initially there would be only pEp nodes.

(anonymity ♡ company, how other people would be incentivated to run
nodes?)

Crypto
------

Modern OpenPGP crypto

(OpenPGP computation might be expensive?)

Deployment diagram
------------------

`Deployment <../../_static/deployment.svg>`_

.. image:: /_static/deployment.svg
