.. _readings:

Readings
========

[BibMixnets]_ contains all relevant papers about Mix networks.

The following papers are more relevant for Mixmailer:

- [Untraceable]_
- [DesignAnonymous]_
- [MixminionPaper]_
- [Trickle]_
- [Batching]_
- [SurveyRouting]_
- [MixCascadesP2P]_
- [Trilemma]_
- [Sphinx]_
- [Loopix]_
- [Stopandgo]_

Recommended by D.:

- [Byzantine]_
- [Uniform]_
- [Gossip]_
